# README #

Image Boxes generation

This program takes the images in a given folder and combines them in a single one by the parameters given to it.
Images are supposed to be previously aligned
Was developed using Python 2.7

Dependencies:
Numpy
OpenCV
OS
SYS

To use use it run:

python folder_name box_width box_height

folder_name would be the folder where are the aligned images
box_width the width in pixels of the images to combine
box_height the height in pixels of the image to combine

The result is an BMP image in the source folder with the name:
folder_nameMixbox_widthxboxheight.bmp